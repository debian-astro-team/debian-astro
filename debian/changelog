debian-astro (4.1) unstable; urgency=medium

  [ Thorsten Alteholz ]
  * add libahp-xc which is needed for indi-eqmod
  * add libahp-gt which is needed for indi-eqmod
  * add c-munipack, a successor of munipack

  [ Ole Streicher ]
  * Add libunity-java to java and vo
  * Fix format of tasks/datareduction

  [ Thorsten Alteholz ]
  * add virtualgps that could be used instead of an external GPS device
  * add blank lines between entries

  [ Ole Streicher ]
  * Update dependencies for latest d/testing

 -- Ole Streicher <olebole@debian.org>  Mon, 30 Oct 2023 10:37:15 +0100

debian-astro (4.0) unstable; urgency=medium

  * Debian Astro release for Debian 12 "Bookworm"

  * Add pymeeus, libopendsp-dev, libopenvlbi-dev, pyorbital
  * Update to latest testing

 -- Ole Streicher <olebole@debian.org>  Thu, 09 Feb 2023 17:49:22 +0100

debian-astro (3.1) unstable; urgency=medium

  [ Thorsten Alteholz ]
  * add libapogee3 to tasks/telescopecontrol
  * add planetary-system-stacker to datareduction
  * add astap and astap-cli to datareduction
  * add openvlbi
  * add some INDI drivers
  * add indi driver

  [ Debian Janitor ]
  * Apply multi-arch hints. + astro-all: Add Multi-Arch: foreign.
  * Trim trailing whitespace.
  * Add missing ${misc:Depends} to Depends for astro-tasks.

  [ Ole Streicher ]
  * Add lapack and freeture to ascl
  * Add ASCL id to python3-pyfftw
  * Add py3-synphot, py3-specreduce
  * Re-organize astropy affiliated packages in tasks
  * Add py3-extinction to ascl
  * Added lib sep-dev to development
  * Add jplephem to python3; cleanup python3 task
  * Add nusolve to radioastronomy
  * Push Standards-Version to 4.6.2. No changes
  * Update to latest testing

 -- Ole Streicher <olebole@debian.org>  Thu, 29 Dec 2022 12:41:27 +0100

debian-astro (3.0) unstable; urgency=medium

  * Debian Astro release for Debian 11 "Bullseye"

  * Add x11iraf packages
  * Add libhealpix-dev to development
  * Add healpy to ASCL; remove outdated WNPPs
  * Add python3-reproject to ascl
  * Add cvsopt, einsteinpy, sep, and astroalign to ascl
  * Add astroalign and cvxopt to python3
  * Update gavo entries in virtual-observatory
  * Add starjava-tjoin to java
  * Remove last remnants of sextractor
  * Update d/control for bullseye

 -- Ole Streicher <olebole@debian.org>  Wed, 24 Feb 2021 10:36:01 +0100

debian-astro (2.2) unstable; urgency=medium

  * Add supersmoother, gatspy, sep, keras, theano, pybdsf, pyerfa to python3
  * Add source-extractor as new name for sextractor
  * Add libcdf-healpix-java, cassis, jsofa to java
  * Remove more Python-2 packages
  * Add pybdsf to radioastronomy
  * Fix theli WNPP number
  * Remove crush: RFP closed
  * Update inkscape call for new inkscape version (Closes: #959594)
  * Push Standards-Version to 4.5.0. No changes needed
  * Push compat to 13. Replace d/compat by debhelper-compat build dep
  * Reformat d/control according to 'cme fix dpkg'

 -- Ole Streicher <olebole@debian.org>  Fri, 15 May 2020 20:49:33 +0200

debian-astro (2.1) unstable; urgency=medium

  * Add einsteinpy, python3-pynpoint, python3-orbit-predictor, heliopy
  * Disable python 2 metapackage and web index entry
  * Add astroscrappy and astrodendro to ascl
  * Update d/control and task-desc

 -- Ole Streicher <olebole@debian.org>  Fri, 13 Sep 2019 11:44:10 +0200

debian-astro (2.0) unstable; urgency=low

  * Debian Astro release for Debian 10 "Buster"

  * Add pandas, skimage, sklearn, hips, poliastro, python3-skyfield,
      PyTorch, specviz, python3-dask, python3-gdl, python3-pygnuplot
      python3-montagepy, bokeh to Python3
  * Remove python 2 variants from tasks
  * Update d/control to reflect Buster freeze

 -- Ole Streicher <olebole@debian.org>  Fri, 15 Feb 2019 17:44:16 +0100

debian-astro (1.4) unstable; urgency=low

  * Update VCS fields to use salsa.d.o
  * Update scisoft to Apr 2018
  * Add vispy ndcube, drms, regions, fast-histogram, mpl-scatter-density
    to python3 (and python)
  * Added cubeviz to viewers
  * Add sptable, fitsutil, mscred to iraf

 -- Ole Streicher <olebole@debian.org>  Fri, 17 Aug 2018 09:48:19 +0200

debian-astro (1.3) unstable; urgency=medium

  [ Steffen Möller ]
  * Add Find_Orb
  * Update tools - find_orb accepted
  * Reordered list of tools.

  [ Ole Streicher ]
  * Create metapackage for IRAF
  * Python tasks cleanup; add astropy-healpix
  * Add Gnuastro to ascl
  * Adjust names of q3c and pgspehere postgres extensions
  * Added splotch to simulation
  * Add Format: line as first line in tasks files
  * Run blends-gen-control (exp. version 0.6.101)

 -- Ole Streicher <olebole@debian.org>  Sun, 25 Mar 2018 22:07:07 +0200

debian-astro (1.2) unstable; urgency=medium

  * Update for newly introduced and announced packages
  * Change VCS location to salsa.d.o
  * Push Standards-Version to 4.1.3

 -- Ole Streicher <olebole@debian.org>  Fri, 05 Jan 2018 20:40:54 +0100

debian-astro (1.1) unstable; urgency=medium

  * Update for newly introduced and announced packages for the buster
    development cycle.

 -- Ole Streicher <olebole@debian.org>  Fri, 23 Jun 2017 11:06:34 +0200

debian-astro (1.0) unstable; urgency=medium

  * Debian Astro release for Debian 9 "Stretch"

  * Add yorick-mira, libgyoto6-dev, python-mvpa2, voro++-dev, starplot
    to tasks packages as Recommends
  * Remove python-pymc from Recommends

 -- Ole Streicher <olebole@debian.org>  Tue, 11 Apr 2017 14:23:31 +0200

debian-astro (0.6) unstable; urgency=medium

  * Add casacore-data, debian-astro-logo, freeture, aravis, gnuastro,
    gwcs, libgnuastro-dev, libsopt-dev, purify-dev, python-drizzle,
    voro+, python3-pysynphot, tablator, munipack, glue-vispy-viewers
  * Remove obsolete gyoto4-dev, pyfits and pywcs
  * Cleanup: remove all unpackaged software without ITP from radioastro

 -- Ole Streicher <olebole@debian.org>  Thu, 08 Dec 2016 17:35:46 +0100

debian-astro (0.5) unstable; urgency=medium

  * ADASS XXVI snapshot release :-)
  * Spellfix in astro-java. Closes: #836895
  * Add package with Debian-Astro Logos
  * Workaround bug #840094 to include all dependencies

 -- Ole Streicher <olebole@debian.org>  Sat, 08 Oct 2016 11:20:38 +0200

debian-astro (0.4) unstable; urgency=medium

  [ Paul Sladen ]
  * Add new metapackage 'virtual-observatory'

  [ Ole Streicher ]
  * Add new metapackage "radioastronomy"
  * Update for newly introduced and announced packages

 -- Ole Streicher <olebole@debian.org>  Mon, 25 Jul 2016 09:50:08 +0200

debian-astro (0.3) unstable; urgency=medium

  * Raise Priority to 'optional'
  * Push standards-version to 3.9.8 (no changes)
  * Extend description of astro-tasks
  * Rebuild with blends-dev 0.6.93. Add new metapackage astro-all

 -- Ole Streicher <olebole@debian.org>  Fri, 15 Apr 2016 17:15:12 +0200

debian-astro (0.2) unstable; urgency=medium

  [ Axel Beckert ]
  * Fix Homepage header. (Closes: #810917)

  [ Ole Streicher ]
  * Add task astro-java
  * Add many new packages and existing Debian packages referenced in the ASCL

 -- Ole Streicher <olebole@debian.org>  Thu, 17 Mar 2016 09:30:54 +0100

debian-astro (0.1) unstable; urgency=medium

  * Initial Release. Closes: #799137

 -- Ole Streicher <olebole@debian.org>  Thu, 17 Dec 2015 09:28:29 +0100
